use std::fs::File;
use open;
use dirs;
use crate::checks::Checks;
use crate::checks::Check;
use std::io::Write as _;

pub fn print_records(records: Checks){

    let current_time = chrono::offset::Local::now();
    let mut date_string: String = current_time.to_string()[0..10].to_string();
    date_string.push_str("-records.csv");

    let mut doc_dir: std::path::PathBuf = dirs::document_dir().unwrap();
    doc_dir.push("rock_dove");
    doc_dir.push(date_string);
    
    let mut file = File::create(&doc_dir).unwrap();
    let mut formatted_string:String = String::new();
    formatted_string.push_str("Count, Date, Payee, Amount, Memo \n");

    for (i, record) in records.list.iter().enumerate(){

        let Check {check_count, date, payee, amount, memo } = record;
        let line = format!("{},{},{},{},{}\n", check_count, date, payee, amount, memo);
        formatted_string.push_str(&line);
    }

    println!("Opening your records which can be found at {}.", &doc_dir.to_str().unwrap());
    let content = formatted_string.as_bytes();
    file.write_all(content).unwrap();
    open::that(&doc_dir);

}