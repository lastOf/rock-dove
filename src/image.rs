extern crate image;

use dirs;
use image::GenericImageView;
use image::{imageops, ImageBuffer, RgbImage};
use std::fs;
use std::io;
use std::path::Path;

pub fn open_image() {
    let mut image_dir: std::path::PathBuf = dirs::config_dir().unwrap();
    image_dir.push("rock_dove");
    image_dir.push("check_image.jpeg");

    println!("Drag an image to the terminal to copy its location.");
    println!("Filename should not include any single quotes or apostraphes such as '.");

    let mut answer = String::new();
    io::stdin()
        .read_line(&mut answer)
        .expect("Failed to read line");

    let mut answer = answer.to_string();
    if answer == "\n" {
        println!("Returning to the main menu.")
    } else {
        answer = answer.trim_end().to_string();

        let answer = answer.replace("'", "");

        // Use the open function to load an image from a Path.
        // `open` returns a `DynamicImage` on success.
        let mut img = image::open(answer).unwrap();

        // The dimensions method returns the images width and height.
        println!("dimensions {:?}", img.dimensions());

        let (width, height) = &img.dimensions();
        let mut final_width = width.to_owned();
        let mut final_height = height.to_owned();
        while final_width < 1785 || final_height < 810 {
            // println!("Was smaller");
            final_height = final_height + height / 2;
            final_width = final_width + width / 2;
        }
        if final_height > height.to_owned() {
            println!("Expanding your image, may take a moment...");
            img = img.resize(
                final_width,
                final_height,
                image::imageops::FilterType::Gaussian,
            );
        }

        // println!("dimensions {:?}", img.dimensions());

        // The color method returns the image's `ColorType`.
        // println!("height: {} width: {}", height, width);
        // println!("{:?}", img.color());
        // let subimg = imageops::crop(&mut img, 0, 0, 400, 1200);

        let img = img.crop(0, 0, 1785, 810);
        let img = img.brighten(150);

        // Write the contents of this image to the Writer in PNG format.
        println!("{} <- is it right?", &image_dir.to_str().unwrap());

        img.save(image_dir).unwrap();
    }
}

pub fn delete_image() {
    let mut image_dir: std::path::PathBuf = dirs::config_dir().unwrap();
    image_dir.push("rock_dove");
    image_dir.push("check_image.jpeg");

    if Path::new(&image_dir).exists() {
        fs::remove_file(image_dir).unwrap();
        println!(
            "
                
        ...Image removed."
        )
    } else {
        println!(
            "
                
        ...No image to remove."
        )
    }
}
