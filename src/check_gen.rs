use dirs;
use english_numbers;
use open;
use printpdf::types::plugins::graphics::two_dimensional::font;
use printpdf::*;
use std::fs::File;
use std::io::BufWriter;
use std::path::Path;

use crate::checks::Check;
use crate::config::Information;

// use std::iter::FromIterator;
const CHECK_HEIGHT: f64 = 69.85;
const CHECK_WIDTH: f64 = 152.4;
const PAPER_HEIGHT: f64 = 279.4;
const PAPER_WIDTH: f64 = 215.9;

pub fn print_check(config: Information, specifics: Check) {
    let file_title = format!("{}.pdf", &specifics.payee);
    let (doc, page1, layer1) =
        PdfDocument::new(file_title, Mm(PAPER_WIDTH), Mm(PAPER_HEIGHT), "Layer 1");
    const MICRO_ENCODING_FONT: &'static [u8] = std::include_bytes!("micrenc.ttf");
    let micrenc = doc.add_external_font(MICRO_ENCODING_FONT).unwrap();

    let medium_font = doc.add_builtin_font(BuiltinFont::TimesBold).unwrap();
    let light_font = doc.add_builtin_font(BuiltinFont::TimesRoman).unwrap();
    let current_layer = doc.get_page(page1).get_layer(layer1);
    // Quadratic shape. The "false" determines if the next (following)
    // point is a bezier handle (for curves)
    // If you want holes, simply reorder the winding of the points to be
    // counterclockwise instead of clockwise.

    let mut image_dir: std::path::PathBuf = dirs::config_dir().unwrap();
    image_dir.push("rock_dove");
    image_dir.push("check_image.jpeg");

    const STARTING_X: f64 = 15.0;
    const STARTING_Y: f64 = PAPER_HEIGHT - 15.0 - CHECK_HEIGHT;

    const DATE_FIELD_X: f64 = STARTING_X + CHECK_WIDTH - 55.0;
    const DATE_FIELD_Y: f64 = STARTING_Y + CHECK_HEIGHT - 15.0;

    const PAY_FIELD_X: f64 = STARTING_X + 15.0;
    const PAY_FIELD_Y: f64 = STARTING_Y + CHECK_HEIGHT / 2.0 + 5.0;

    const AMOUNT_FIELD_X: f64 = STARTING_X + 15.0;
    const AMOUNT_FIELD_Y: f64 = STARTING_Y + CHECK_HEIGHT / 2.0 - 5.0;

    let pay_points = vec![
        (Point::new(Mm(PAY_FIELD_X), Mm(PAY_FIELD_Y)), false),
        (Point::new(Mm(CHECK_WIDTH - 20.0), Mm(PAY_FIELD_Y)), false),
    ];

    let pay_line = Line {
        points: pay_points,
        is_closed: false,
        has_fill: false,
        has_stroke: true,
        is_clipping_path: false,
    };

    let amount_points = vec![
        (Point::new(Mm(AMOUNT_FIELD_X), Mm(AMOUNT_FIELD_Y)), false),
        (
            Point::new(Mm(CHECK_WIDTH - 20.0), Mm(AMOUNT_FIELD_Y)),
            false,
        ),
    ];

    let amount_line = Line {
        points: amount_points,
        is_closed: false,
        has_fill: false,
        has_stroke: true,
        is_clipping_path: false,
    };

    let date_points = vec![
        (Point::new(Mm(DATE_FIELD_X), Mm(DATE_FIELD_Y)), false),
        (
            Point::new(Mm(CHECK_WIDTH - 10.0), Mm(STARTING_Y + CHECK_HEIGHT - 15.0)),
            false,
        ),
    ];

    let date_line = Line {
        points: date_points,
        is_closed: false,
        has_fill: false,
        has_stroke: true,
        is_clipping_path: false,
    };

    let amount_borders = vec![
        (
            Point::new(Mm(CHECK_WIDTH - 13.0), Mm(PAY_FIELD_Y + 7.0)),
            false,
        ),
        (
            Point::new(Mm(CHECK_WIDTH - 13.0), Mm(PAY_FIELD_Y - 1.0)),
            false,
        ),
        (
            Point::new(Mm(CHECK_WIDTH + 6.0), Mm(PAY_FIELD_Y - 1.0)),
            false,
        ),
        (
            Point::new(Mm(CHECK_WIDTH + 6.0), Mm(PAY_FIELD_Y + 7.0)),
            false,
        ),
    ];

    let amount_frame = Line {
        points: amount_borders,
        is_closed: true,
        has_fill: true,
        has_stroke: false,
        is_clipping_path: false,
    };

    let check_borders = vec![
        (Point::new(Mm(STARTING_X), Mm(STARTING_Y)), false),
        (
            Point::new(Mm(STARTING_X), Mm(STARTING_Y + CHECK_HEIGHT)),
            false,
        ),
        (
            Point::new(Mm(STARTING_X + CHECK_WIDTH), Mm(STARTING_Y + CHECK_HEIGHT)),
            false,
        ),
        (
            Point::new(Mm(STARTING_X + CHECK_WIDTH), Mm(STARTING_Y)),
            false,
        ),
    ];
    // Is the shape stroked? Is the shape closed? Is the shape filled?
    let check_frame = Line {
        points: check_borders,
        is_closed: true,
        has_fill: true,
        has_stroke: true,
        is_clipping_path: false,
    };

    let back_check_points = vec![
        (
            Point::new(Mm(STARTING_X), Mm(STARTING_Y - CHECK_HEIGHT)),
            false,
        ),
        (
            Point::new(Mm(STARTING_X), Mm(STARTING_Y - CHECK_HEIGHT + CHECK_HEIGHT)),
            false,
        ),
        (
            Point::new(
                Mm(STARTING_X + CHECK_WIDTH),
                Mm(STARTING_Y - CHECK_HEIGHT + CHECK_HEIGHT),
            ),
            false,
        ),
        (
            Point::new(Mm(STARTING_X + CHECK_WIDTH), Mm(STARTING_Y - CHECK_HEIGHT)),
            false,
        ),
    ];
    // Is the shape stroked? Is the shape closed? Is the shape filled?
    let back_frame = Line {
        points: back_check_points,
        is_closed: true,
        has_fill: true,
        has_stroke: true,
        is_clipping_path: false,
    };

    const FOR_FIELD_X: f64 = STARTING_X + 15.0;
    const FOR_FIELD_Y: f64 = STARTING_Y + 15.0;

    const SIGNATURE_FIELD_X: f64 = STARTING_X + 105.0;
    const SIGNATURE_FIELD_Y: f64 = STARTING_Y + 15.0;

    let for_points = vec![
        (Point::new(Mm(FOR_FIELD_X), Mm(FOR_FIELD_Y)), false),
        (Point::new(Mm(FOR_FIELD_X + 60.0), Mm(FOR_FIELD_Y)), false),
    ];

    let for_line = Line {
        points: for_points,
        is_closed: false,
        has_fill: false,
        has_stroke: true,
        is_clipping_path: false,
    };

    let signature_points = vec![
        (
            Point::new(Mm(SIGNATURE_FIELD_X), Mm(SIGNATURE_FIELD_Y)),
            false,
        ),
        (
            Point::new(Mm(SIGNATURE_FIELD_X + 40.0), Mm(SIGNATURE_FIELD_Y)),
            false,
        ),
    ];

    let signature_line = Line {
        points: signature_points,
        is_closed: false,
        has_fill: false,
        has_stroke: true,
        is_clipping_path: false,
    };

    let routing_frame = vec![
        (
            Point::new(Mm(STARTING_X + 5.0), Mm(STARTING_Y + 5.0)),
            false,
        ),
        (
            Point::new(Mm(STARTING_X + 5.0), Mm(STARTING_Y + 12.0)),
            false,
        ),
        (
            Point::new(Mm(STARTING_X + CHECK_WIDTH - 5.0), Mm(STARTING_Y + 12.0)),
            false,
        ),
        (
            Point::new(Mm(STARTING_X + CHECK_WIDTH - 5.0), Mm(STARTING_Y + 5.0)),
            false,
        ),
    ];

    let routing_line = Line {
        points: routing_frame,
        is_closed: false,
        has_fill: true,
        has_stroke: true,
        is_clipping_path: false,
    };

    let endorse_points = vec![
        (
            Point::new(Mm(CHECK_WIDTH), Mm(STARTING_Y - 80.0 + CHECK_HEIGHT)),
            false,
        ),
        (
            Point::new(Mm(CHECK_WIDTH), Mm(STARTING_Y - CHECK_HEIGHT + 10.0)),
            false,
        ),
    ];

    let endorse_line = Line {
        points: endorse_points,
        is_closed: false,
        has_fill: false,
        has_stroke: true,
        is_clipping_path: false,
    };

    let fill_color = Color::Cmyk(Cmyk::new(0.0, 0.05, 0.07, 0.07, None));
    let outline_color = Color::Cmyk(Cmyk::new(0.0, 0.67, 0.91, 0.87, None));
    let mut dash_pattern = LineDashPattern::default();
    dash_pattern.dash_1 = Some(20);
    current_layer.set_fill_color(fill_color);
    current_layer.set_outline_color(outline_color);
    current_layer.set_outline_thickness(1.0);

    // Draw first line
    current_layer.add_shape(check_frame);
    current_layer.add_shape(back_frame);

    // fn check_image ()-> std::io::Result<()>{}

    if Path::new(&image_dir).exists() {
        let mut image_file = File::open(image_dir).unwrap();
        let image =
            Image::try_from(image::jpeg::JpegDecoder::new(&mut image_file).unwrap()).unwrap();

        // translate x, translate y, rotate, scale x, scale y
        // by default, an image is optimized to 300 DPI (if scale is None)
        // rotations and translations are always in relation to the lower left corner
        image.add_to_layer(
            current_layer.clone(),
            Some(Mm(15.65)),
            Some(Mm(195.1)),
            None,
            None,
            None,
            None,
        );
    }

    let black = Color::Rgb(Rgb::new(0.0, 0.0, 0.0, None));
    current_layer.set_outline_color(black);
    let white = Color::Rgb(Rgb::new(255.0, 255.0, 255.0, None));
    current_layer.set_fill_color(white);

    current_layer.add_shape(date_line);
    current_layer.add_shape(pay_line);
    current_layer.add_shape(amount_line);
    current_layer.add_shape(for_line);
    current_layer.add_shape(signature_line);
    current_layer.add_shape(amount_frame);
    current_layer.add_shape(routing_line);
    current_layer.add_shape(endorse_line);

    let font_color = Color::Cmyk(Cmyk::new(0.0, 0.0, 0.0, 100.0, None));
    let outline_color_2 = Color::Greyscale(Greyscale::new(0.45, None));

    current_layer.set_fill_color(font_color);
    current_layer.set_outline_color(outline_color_2);

    let mut routing_str = String::new();
    routing_str.push('a');
    routing_str.push_str(&config.institution.routing);
    routing_str.push('a');

    let mut acc_str = String::new();
    acc_str.push_str(&config.institution.account);
    acc_str.push('c');

    current_layer.use_text(
        routing_str,
        16 as f64,
        Mm(25.0),
        Mm(PAPER_HEIGHT - 8.0 - CHECK_HEIGHT),
        &micrenc,
    );

    current_layer.use_text(
        acc_str,
        16 as f64,
        Mm(80.0),
        Mm(PAPER_HEIGHT - 8.0 - CHECK_HEIGHT),
        &micrenc,
    );

    current_layer.use_text(
        "Date",
        12 as f64,
        Mm(DATE_FIELD_X + 11.0),
        Mm(DATE_FIELD_Y - 3.5),
        &light_font,
    );
    current_layer.use_text(
        "To the order of",
        8 as f64,
        Mm(AMOUNT_FIELD_X - 10.0),
        Mm(AMOUNT_FIELD_Y + 4.0),
        &light_font,
    );
    current_layer.use_text(
        "Dollars",
        12 as f64,
        Mm(CHECK_WIDTH - 19.0),
        Mm(AMOUNT_FIELD_Y),
        &light_font,
    );
    current_layer.use_text(
        "Pay",
        14 as f64,
        Mm(PAY_FIELD_X - 10.0),
        Mm(PAY_FIELD_Y),
        &medium_font,
    );
    current_layer.use_text(
        "For",
        12 as f64,
        Mm(FOR_FIELD_X - 6.0),
        Mm(FOR_FIELD_Y),
        &light_font,
    );
    current_layer.use_text(
        "Signature",
        12 as f64,
        Mm(SIGNATURE_FIELD_X - 18.0),
        Mm(SIGNATURE_FIELD_Y),
        &light_font,
    );
    current_layer.use_text(
        "$",
        18 as f64,
        Mm(CHECK_WIDTH - 17.5),
        Mm(PAY_FIELD_Y + 1.0),
        &medium_font,
    );

    let mut addr_string = config.address.city;
    addr_string.push(',');
    addr_string.push(' ');
    addr_string.push_str(&config.address.state);
    addr_string.push(' ');
    addr_string.push_str(&config.address.zip);

    let check_num: u32 = specifics.check_count;
    let check_num_str = format!("Check #{:03}", check_num);
    current_layer.use_text(
        check_num_str,
        8 as f64,
        Mm(CHECK_WIDTH - 8.0),
        Mm(STARTING_Y + CHECK_HEIGHT - 10.0),
        &light_font,
    );

    current_layer.use_text(
        &specifics.payee,
        16 as f64,
        Mm(PAY_FIELD_X + 4.0),
        Mm(PAY_FIELD_Y + 1.0),
        &medium_font,
    );

    current_layer.use_text(
        &specifics.date,
        14 as f64,
        Mm(DATE_FIELD_X + 5.0),
        Mm(DATE_FIELD_Y + 1.0),
        &medium_font,
    );
    current_layer.use_text(
        specifics.memo,
        12 as f64,
        Mm(FOR_FIELD_X + 1.0),
        Mm(FOR_FIELD_Y + 1.0),
        &light_font,
    );

    if config.business == "" {
        current_layer.use_text(
            config.full_name,
            16 as f64,
            Mm(STARTING_X + 8.0),
            Mm(STARTING_Y + CHECK_HEIGHT - 10.0),
            &medium_font,
        );
        current_layer.use_text(
            config.address.street_addr,
            12 as f64,
            Mm(STARTING_X + 8.0),
            Mm(STARTING_Y + CHECK_HEIGHT - 15.0),
            &light_font,
        );
        current_layer.use_text(
            addr_string,
            12 as f64,
            Mm(STARTING_X + 8.0),
            Mm(STARTING_Y + CHECK_HEIGHT - 20.0),
            &light_font,
        );
    } else {
        current_layer.use_text(
            config.business,
            16 as f64,
            Mm(STARTING_X + 8.0),
            Mm(STARTING_Y + CHECK_HEIGHT - 10.0),
            &medium_font,
        );
        current_layer.use_text(
            config.full_name,
            12 as f64,
            Mm(STARTING_X + 8.0),
            Mm(STARTING_Y + CHECK_HEIGHT - 15.0),
            &medium_font, 
        );
        current_layer.use_text(
            config.address.street_addr,
            10 as f64,
            Mm(STARTING_X + 8.0),
            Mm(STARTING_Y + CHECK_HEIGHT - 19.0),
            &light_font,
        );
        current_layer.use_text(
            addr_string,
            10 as f64,
            Mm(STARTING_X + 8.0),
            Mm(STARTING_Y + CHECK_HEIGHT - 23.0),
            &light_font,
        );
    }

    if config.institution.name != "" {
        current_layer.use_text(
            config.institution.name,
            6 as f64,
            Mm(PAY_FIELD_X),
            Mm(AMOUNT_FIELD_Y - 4.0),
            &light_font,
        );

        let mut institute_address_string = String::new();

        institute_address_string.push_str(&config.institution.mailing_address.street_addr);
        institute_address_string.push(' ');
        institute_address_string.push_str(&config.institution.mailing_address.city);
        institute_address_string.push(',');
        institute_address_string.push(' ');
        institute_address_string.push_str(&config.institution.mailing_address.state);
        institute_address_string.push(' ');
        institute_address_string.push_str(&config.institution.mailing_address.zip);

        current_layer.use_text(
            institute_address_string,
            5.5,
            Mm(PAY_FIELD_X),
            Mm(AMOUNT_FIELD_Y - 6.0),
            &light_font,
        );
        current_layer.use_text(
            config.institution.website,
            5.5 as f64,
            Mm(PAY_FIELD_X),
            Mm(AMOUNT_FIELD_Y - 7.5),
            &light_font,
        );
    }
    let payment_amount: f64 = specifics.amount.parse::<f64>().unwrap();
    let payment_amount = f64::trunc(payment_amount * 100.0) / 100.0;

    let mut amount_string_font_size = 12.0;

    if payment_amount != 0.0 {
        
        let payment_amount = format!("{}", payment_amount);
        let mut split_amount = payment_amount.split('.');
        let dollars = split_amount.next().unwrap().parse::<i64>().unwrap();
        let dollars_string = english_numbers::convert_all_fmt(dollars);
        let cents = split_amount.next().unwrap_or_else(|| "00");
        let cents_padded = format!("{:0<2}", &cents);
        let cents = cents_padded.parse::<i64>().unwrap();
        let cents_string = english_numbers::convert_all_fmt(cents);
        let payment_amount = format!("{}.{}", dollars, cents_padded);

        let amount_string = dollars_string + " dollars and " + &cents_string + " cents";
        println!("string length: {}", amount_string.len());
        if amount_string.len() > 35 {
            amount_string_font_size = 10.0
        };
        if amount_string.len() > 75 {
            amount_string_font_size = 7.0
        };
        


        current_layer.use_text(
            if dollars > 0 || cents > 0 {
                &payment_amount
            } else {
                ""
            },
            12 as f64,
            Mm(CHECK_WIDTH - 13.0),
            Mm(PAY_FIELD_Y + 1.0),
            &light_font,
        );
        current_layer.use_text(
            if dollars > 0 || cents > 0 {
                &amount_string
            } else {
                ""
            },
            amount_string_font_size as f64,
            Mm(STARTING_X + 25.0),
            Mm(AMOUNT_FIELD_Y + 1.0),
            &light_font,
        );
    }

    let mut doc_dir: std::path::PathBuf = dirs::document_dir().unwrap();
    doc_dir.push("rock_dove");
    if !std::path::Path::new(&doc_dir).exists() {
        std::fs::create_dir(&doc_dir).unwrap();
    }
    let filename_contents = if specifics.payee.trim().is_empty() {
        "blank"
    } else {
        &specifics.payee
    }.to_string();
    

    let file_name = format!(
        "{}_{}_{}.pdf",
        check_num,
        str::replace(&filename_contents, " ", "_"),
        str::replace(&specifics.date, "/", "_")
    );
    doc_dir.push(file_name.as_str());
    doc.save(&mut BufWriter::new(File::create(&doc_dir).unwrap()))
        .unwrap();
    open::that(&doc_dir).unwrap();

    println!(
        "
    
    Your check will be found at: {}
    Enjoy!",
        &doc_dir.to_str().unwrap()
    );
}
