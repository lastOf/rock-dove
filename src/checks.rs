use chrono;
use dirs;
use serde::{Deserialize, Serialize};
use std::fs::File;

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Checks {
    pub list: Vec<Check>,
}

impl Checks {
    pub fn load() -> Checks {
        let mut records_path = dirs::data_dir().unwrap();
        records_path.push("rock_dove_data");
        // println!("{}", &records_path.to_str().unwrap());
        let records_file = File::open(&records_path);
        let records_file = match records_file {
            Ok(file) => file,
            _ => {
                let default: Vec<Check> = vec![];
                bincode::serialize_into(
                    File::create(&records_path).unwrap(),
                    &default, //fix this
                )
                .unwrap();
                let replacement = File::open(&records_path);

                replacement.unwrap()
            }
        };
        let records_info: Checks = match bincode::deserialize_from(records_file) {
            Ok(val) => val,
            _ => Checks { list: vec![] },
        };
        records_info
    }

    pub fn add_record(
        payee: String,
        amount: f64,
        memo: String,
        mut records_info: Checks,
    ) -> Checks {
        let check_count: u32 = records_info.list.len() as u32 + 1;
        let fresh_check = Check::new_check(payee, amount, memo, check_count);
        records_info.list.push(fresh_check);
        Checks::save(&records_info);
        records_info
    }

    pub fn save(current_records: &Checks) {
        let mut records_path = dirs::data_dir().unwrap();
        records_path.push("rock_dove_data");
        bincode::serialize_into(
            File::create(&records_path).unwrap(),
            &current_records, //fix this
        )
        .unwrap();
    }
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Check {
    pub payee: String,
    pub amount: String,
    pub memo: String,
    pub date: String,
    pub check_count: u32,
}

impl Check {
    fn new_check(payee: String, amount: f64, memo: String, check_count: u32) -> Check {
        let payment_amount: f64 = amount;
        let payment_amount = f64::trunc(payment_amount * 100.0) / 100.0;

        let payment_amount = format!("{}", payment_amount);
        let mut split_amount = payment_amount.split('.');
        let dollars = split_amount.next().unwrap().parse::<i64>().unwrap();
        let cents = split_amount.next().unwrap_or_else(|| "00");
        let cents_padded = format!("{:0<2}", &cents);
        let amount = format!("{}.{}", dollars, cents_padded);

        let current_time = chrono::offset::Local::now();
        let date_string: String = current_time.to_string()[0..10].to_string();
        let mut date_arr = date_string.split('-');
        let year = date_arr.next().unwrap();
        let month = date_arr.next().unwrap();
        let day = date_arr.next().unwrap();
        let date = format!("{}/{}/{}", month, day, year);
        Check {
            payee,
            amount,
            memo,
            date,
            check_count,
        }
    }
}
