use bincode;
use crossterm::{
    event::{self, Event as CEvent, KeyCode},
    terminal::{disable_raw_mode, enable_raw_mode},
};
use serde::{Deserialize, Serialize};

use dirs;
use std::fs;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;
use std::sync::mpsc;
use std::thread;
use std::time::{Duration, Instant};
use tui::{
    backend::CrosstermBackend,
    layout::{Alignment, Constraint, Direction, Layout},
    style::{Color, Modifier, Style},
    text::{Span, Spans},
    widgets::{
        Block, BorderType, Borders, Cell, List, ListItem, ListState, Paragraph, Row, Table, Tabs,
    },
    Terminal,
};

extern crate open;
mod check_gen;
mod checks;
mod config;
mod image;
mod records;

enum Event<I> {
    Input(I),
    Tick,
}

fn main() {
    let mut config_path: std::path::PathBuf = dirs::config_dir().unwrap();
    config_path.push("rock_dove");
    fs::create_dir_all(&config_path).unwrap();
    config_path.push("rock_dove_config");
    println!("{}", &config_path.to_str().unwrap());
    let config_file = File::open(&config_path);
    let mut config_empty = false;
    let config_file = match config_file {
        Ok(file) => file,
        _ => {
            config_empty = true;
            bincode::serialize_into(
                File::create(&config_path).unwrap(),
                &config::Information::default(),
            )
            .unwrap();
            let replaced = File::open(&config_path).unwrap();
            replaced
        }
    };
    let mut config_info: config::Information =
        match bincode::deserialize_from::<_, config::Information>(config_file) {
            Ok(val) => {
                println!("{}", val.full_name);
                val
            }
            _ => {
                config_empty = true;
                config::Information::default()
            }
        };

    println!(
        "
    @@@@@@@@@@@@@@@@ Rock_Dove @@@@@@@@@@@@@@@@@@@@@@@
    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    @@@@@@@@@@@@@@@@QDSIT|??sabQ@@@@@@@@@@@@@@@@@@@@@@
    @@@@@@@@@@@@@Dz;'```````````.~q@@@@@@@@@@@@@@@@@@@
    @@@@@@@@@@Q5~.``````````````````_uQ@@@@@@@@@@@@@@@
    @@@@@@@@@N~`````````````.~iv^'````,S@@@@@@@@@@@@@@
    @@@@@@@@6,``````````````<@@@D_`````.i@@@@@@@@@@@@@
    @@@@@@@q'```````````````;%@@q,```````|Q@@@@@@@@@@@
    @@@@@@Q~`````````````````.,~'``````    .,^7PQ@@@@@
    @@@@@@y``````````````````````````           'iQ@@@
    @@@@@@v````````````````````````````   `~+zkB@@@@@@
    @@@@@@i```````````````````````````````:W@@@@@@@@@@
    @@@@@@L````````````````````````````````~b@@@@@@@@@
    @@@@@@?`````````````````````````````````'7Q@@@@@@@
    @@@@@@<```````````````````````````````````~E@@@@@@
    @@@@@@=````````````````````````````````````.!ERQQ@
    @@@@@@=`````````````````````````````````````````'_
    @@@@@@=```````````````````````````````````````````
    @@@@@@>```````````````````````````````````````````
    @@@@@@*```````````````````````````````````````````
    @@@@@@|```````````````````````````````````````````
    @@@@@@i```````````````````````````````````````````
    @@@@@@7```````````````````````````````````````````
    @@@@@@I```````````````````````````````````````````
    @@@@@@y```````````````````````````````````````````
    @@@@@@y.``````````````````````````````````````````
    "
    );

    println!(
        "
        
    Welcome to Rock Dove!"
    );
    println!("");
    if config_empty {
        println!("First we need to get you set up.");
        println!("");

        config_info = config_setup(config_info, &config_path);
    } else {
        println!(
            "
        Here is what you have on record currently:
        Name: {}
        Business: {}
        Address: {} {} {}, {}
        Institution Name: {}
                    Routing: {}
                    account: {}
                    Street Address: {}
                    City: {}
                    State: {}
                    Zip: {}
                    Website: {}
        ",
            config_info.full_name,
            config_info.business,
            config_info.address.street_addr,
            config_info.address.city,
            config_info.address.state,
            config_info.address.zip,
            config_info.institution.name,
            config_info.institution.routing,
            config_info.institution.account,
            config_info.institution.mailing_address.street_addr,
            config_info.institution.mailing_address.city,
            config_info.institution.mailing_address.state,
            config_info.institution.mailing_address.zip,
            config_info.institution.website,
        )
    }

    options_menu(config_info, &config_path);

    fn options_menu(mut config_info: config::Information, config_path: &std::path::PathBuf) {
        println!(
            "Type \"blank\" to create a new blank check (Without a pre-filled amount or payee).
            "
        );
        println!(
            "Type \"check\" to add payment details and create a new check.
        "
        );
        println!(
            "Type \"info\" to edit your information.
        "
        );
        println!(
            "Type \"image\" to add an image to the background of the front side of the check.
            "
        );
        let mut image_dir: std::path::PathBuf = dirs::config_dir().unwrap();
        image_dir.push("rock_dove");
        image_dir.push("check_image.jpeg");
        if Path::new(&image_dir).exists() {
            println!(
                "
            Type \"delete image\" to remove the image, and future checks will return to the default.
            "
            );
        };

        println!(
            "Type \"records\" to create a csv(spreadsheet) of your check records.
        "
        );
        println!(
            "Type \"exit\" to exit the program.
        "
        );
        let mut yes_or_no = String::new();
        io::stdin()
            .read_line(&mut yes_or_no)
            .expect("Failed to read line");
        match yes_or_no.as_str() {
            "blank\n" => {
                println!("Great");
                let specifics = checks::Checks::add_record(
                    " ".to_owned(),
                    0.0,
                    " ".to_owned(),
                    checks::Checks::load(),
                );
                println!("{:?}", &specifics);
                check_gen::print_check(
                    config_info.clone(),
                    specifics.list.iter().last().unwrap().to_owned(),
                );
            }
            "check\n" => {
                let vec_of_info = ["Who is the check for?", "How much?", "Note?"];
                let mut payee = String::new();
                let mut amount = 0.0;
                let mut memo = String::new();
                for (i, question) in vec_of_info.iter().enumerate() {
                    println!("{}", question);
                    let mut answer = String::new();
                    io::stdin()
                        .read_line(&mut answer)
                        .expect("Failed to read line");
                    let mut answer = answer.to_string();
                    let len = answer.len();
                    answer.truncate(len - 1);
                    match i {
                        0 => payee = answer,
                        1 => amount = answer.parse::<f64>().unwrap(),
                        2 => memo = answer,
                        _ => println!("oops!"),
                    }
                    // Put the data somewhere in order to get it into the Information struct.
                }
                let specifics =
                    checks::Checks::add_record(payee, amount, memo, checks::Checks::load());
                println!("{:?}", &specifics);
                check_gen::print_check(
                    config_info.clone(),
                    specifics.list.iter().last().unwrap().to_owned(),
                );
            }
            "info\n" => {
                config_info = config_setup(config_info, &config_path);
                options_menu(config_info, &config_path);
            }
            "image\n" => {
                image::open_image();
                options_menu(config_info, &config_path);
            }
            "delete image\n" => {
                image::delete_image();
                options_menu(config_info, &config_path);
                println!(
                    "
                
                ...Image removed."
                )
            }
            "records\n" => {
                records::print_records(checks::Checks::load());
            }
            "exit\n" => {
                println!("Goodbye")
            }
            _ => {
                println!("Uknown command, your options are: ");
                options_menu(config_info, &config_path);
            }
        }
    }
    // let mut results: [String; 13];
}
fn config_setup(
    mut config_info: config::Information,
    config_path: &std::path::PathBuf,
) -> config::Information {
    let vec_of_questions = [
        "What is your full name?",
        "(Business Name) If your checks are coming from your business list the business name here:",
        "What is your street address?",
        "What is your City?",
        "What is your State?",
        "What is your Zip Code?",
        "What is your bank's name?",
        "What is your bank's street address or PO Box?",
        "What is your bank's City?",
        "What is your bank's State?",
        "What is your bank's Zip Code?",
        "What is your bank's website?",
        "What is your banks routing number?",
        "What is your account number?",
    ];

    println!(
        "Lets populate the information that will be consistent on your checks.
    "
    );

    println!(
        "Things like your address, bank info, and routing number.
    "
    );

    println!(
        "This information will only be stored locally on your computer.
    "
    );

    println!(
        "To keep a fields current value just press 'enter'.
    "
    );

    for (i, question) in vec_of_questions.iter().enumerate() {
        println!("{}", question);
        let mut answer = String::new();
        io::stdin()
            .read_line(&mut answer)
            .expect("Failed to read line");

        let mut answer = answer.to_string();
        if answer == "\n" {
            continue;
        }
        let len = answer.len();
        answer.truncate(len - 1);

        match i {
            0 => config_info.full_name = answer,
            1 => config_info.business = answer,
            2 => config_info.address.street_addr = answer,
            3 => config_info.address.city = answer,
            4 => config_info.address.state = answer,
            5 => config_info.address.zip = answer,
            6 => config_info.institution.name = answer,
            7 => config_info.institution.mailing_address.street_addr = answer,
            8 => config_info.institution.mailing_address.city = answer,
            9 => config_info.institution.mailing_address.state = answer,
            10 => config_info.institution.mailing_address.zip = answer,
            11 => config_info.institution.website = answer,
            12 => config_info.institution.routing = answer,
            13 => config_info.institution.account = answer,
            _ => println!("oops!"),
        };
        bincode::serialize_into(File::create(config_path).unwrap(), &config_info).unwrap();
        // Put the data somewhere in order to get it into the Information struct.
    }
    config_info
}
