use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Information {
    pub full_name: String,
    pub business: String,
    pub address: Address,
    pub institution: Institution,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Institution {
    pub name: String,
    pub mailing_address: Address,
    pub website: String,
    pub routing: String,
    pub account: String,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Address {
    pub street_addr: String,
    pub city: String,
    pub state: String,
    pub zip: String,
}
