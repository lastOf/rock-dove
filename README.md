# Rock Dove

![image info](./rock_dove.png)

Generate printable check (currently implementing the format common in the US)pdfs which are able to be deposited with the mobile deposit functionality contained with most banking mobile apps these days.

---

## Project Aims

- The app should be accessible on linux, mac and windows.
- Built in Rust; I hope to advance in understanding as much as possible on best practices in rust.
- Intuitive and straightforward to use. Work out of the box, without the user needing to look anything up, such as commands.
- Save general information persistently (name, address, bank, routing number acc number)
- Keep a record of checks that have been generated in a manner that would be easy to review, or produce a document that would be easy to review.
- Edit persistent information if needed
- Add a background image without impairing the functioning of the check. (the mobile app being able to read it, probably reduced opacity)

## Todo

- [x] Add business name line
- [x] Add bank institution information
- [x] Adjust dollar sign
- [x] Center the dollar amount
- [x] Make font size for large dollar values phrases more specific
- [x] Pick a simpler color
- [x] Can anything be done to refine the image processing?
- [] Handle the filename for blank checks better.
- [x] Fix Images not working for release build.
- [] Custom error handling. Dont just unwrap everything =D.
- [] Add crate for creating a package that I used in the snake app.
- [] Package it for linux, mac, windows
- [] Add extra information and instructions to the pdf
   - Rockdove logo and website
   - Cut and fold
   - Deposit with mobile check depositing app


## Description of logic to implement:

1. Welcome the user in the CLI, which some explanation for orientation.
1. On startup check if there is already record of personal information.
1. If some or all records are missing, prompt the user to fill the missing fields out.
1. List all completed fields.
1. Prompt user with multiple options
   1. Generate a check.
   1. review history.
   1. edit details.
   1. add an image

To decide:

How do i implement the multiple choice options cli prompt?

tui-rs

[example](https://blog.logrocket.com/rust-and-tui-building-a-command-line-interface-in-rust/)

Is it possible to increase the font size of the cli programmatically, I feel like it would be less intimidating, and easier for the average user to orient themselves.

Should the user be able to choose a location to send saved checks? Where is an ideal default?

### Generate a check

1. Generate a pdf using [printpdf](https://crates.io/crates/printpdf).
   1. Populate with data
   1. [Generate english numbers](https://crates.io/crates/english-numbers) for amount line.
   1. Use the microencoding font to encode the routing information
   1. add image
   1. save the pdf.
1. Open the pdf using the [open crate](https://crates.io/crates/open)
1. return to main menu

To decide:

**What is the best way to include the font?** I see that you can load files into the binary itself. I wouldnt want the font file to be deletable because the checks wont deposit without that font so it would break the app.

Where checks are saved.

- create a folder on the desktop.
- documents
- Next to the check printing package

### Review history

Not sure the ideal way to display this. Since the command line is not the most comfortable place for everyone, it may be good to just generate a pdf containing the records on each call, overwriting the previous file(?). Then opening the pdf immediately. This would give some formatting and be easier to read and store for business or personal records etc. Seperating the data storage method from the display method, means we can use an ideal option for each.

### Edit details

1. Call the same prompt from the start, but ensuring we go through each option.
1. List the resulting details
1. return to main menu

### Add an image

Not sure the best way to implement this. I would want it to be as straightforward as possible for the user to input the image, so that no one gets overwhelmed by trying to find what the path is or something. But then, take that image and store it in the package where the app can reliably find it. Once we move it we should reduce the opacity. Need to find a crate for that.

Also have to implement a remove image, if the person wants to return to a blank check.

## Related Resources:

https://money.stackexchange.com/questions/73684/how-can-i-print-my-own-checks-on-my-printer-on-regular-paper

https://money.stackexchange.com/questions/47441/is-there-any-law-prohibiting-the-use-of-a-home-made-check
